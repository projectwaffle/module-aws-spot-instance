# AWS EC2 SpotFleet of instances Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-spot-instance.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-spot-instance/)

Terraform module which creates EC2 Fleet of Instaces and related resources on AWS. Might be used for different modules

These types of resources are supported:
* [Spot Fleet Reqest](https://www.terraform.io/docs/providers/aws/r/spot_fleet_request.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "spot" {
  source = "bitbucket.org/projectwaffle/module-aws-spot-instance.git?ref=tags/v0.0.1"

  name = "training-prod"

  resource_identities = {}

  global_tags = "${var.global_tags}"

  create_spot = "true"

  target_capacity        = "0"
  spot_price             = "0.0018"
  vpc_security_group_ids = ["${module.sg_spot.security_group_id}"]
  subnet_id              = "${module.vpc.public_subnets}"
  subnet_az              = "${module.vpc.public_subnets_azs}"
  iam_fleet_role         = "${module.spot_fleet_assume_role.assume_role_arn}"
  iam_instance_profile   = "${module.spot_ec2_assume_role.assume_role_instance_profile_name}"
  key_name               = "training-keypair-ireland"
}

module "spot_ec2_assume_role" {
  source = "./module-aws-iam"

  resource_identities = {
    assume_role = "spot-ec2-role"
  }

  name               = "training-prod"
  create_assume_role = "true"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "ec2.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "iam:PassRole,ec2:DescribeImages,ec2:DescribeSubnets,ec2:RequestSpotInstances,ec2:TerminateInstances,ec2:DescribeInstanceStatus,ec2:CreateTags"
      statement_resources = "*"
    },
  ]
}

module "spot_fleet_assume_role" {
  source = "./module-aws-iam"

  resource_identities = {
    assume_role = "spot-fleet-role"
  }

  name               = "training-prod"
  create_assume_role = "true"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "spotfleet.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "ec2:DescribeImages,ec2:DescribeSubnets,ec2:RequestSpotInstances,ec2:TerminateInstances,ec2:DescribeInstanceStatus,am:PassRole,elasticloadbalancing:RegisterTargets"
      statement_resources = "*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "elasticloadbalancing:RegisterInstancesWithLoadBalancer"
      statement_resources = "arn:aws:elasticloadbalancing:*:*:loadbalancer/*"
    },
  ]

  assume_role_policy_condition = [
    {
      statement_efect     = "Allow"
      statement_actions   = "iam:PassRole"
      statement_resources = "*"
      condition_test      = "StringEquals"
      condition_variable  = "am:PassedToService"
      condition_values    = "ec2.amazonaws.com,ec2.amazonaws.com.cn"
    },
  ]
}

module "sg_spot" {
  source = "./module-aws-security-group"

  resource_identities = {
    security_group = "sg-spot"
  }

  name = "training-prod"

  global_tags           = "${var.global_tags}"
  create_security_group = "true"
  vpc_id                = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "Allow Port SSH"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow Any"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

```

Security group, subnet ID and Subnet AZ are imported from different modules. Instance profile as well. Can be specified just IDs

## Custom Userdata and Instance hostname

By default, module will create userdata to setup next default settings:

 * Set up hostname - useing variable `instance_name_prefix = "app-p"`
 * Do update to all software (amazon linux only)

There is an option to add custom userdata script with next:
```hcl
module "instance" {

  create_custom_userdata      = "true"
  custom_userdata_script_path = "./custom_script.sh"
}
```

Just point to custome script file.

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"`
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    config_config_rule            = "config-rule"
    config_configuration_recorder = "config-recorder"
  }
```

Both variables should be declared in module section:
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    instance            = "app"
  }
  ......
}
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["instance"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["instance"],)), var.global_tags, var.module_tags, var.instance_tags)}"
```

## Resource Tagging

There are three level of resource tagging declaration

* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "instance" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "instance_tags" {
  description = "Custome Instance Tags"
  default     = {
    role = "app"
  }
}
```

Combination of all this variables should cover all the tagging requirements.
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Key Pair management

EC2 instances need keypair to connect to SSH (linux) or generate Administrator password (Window system).
There are couple of ways to manage Keypair

* Variable called `key_name` - to use existing AWS Console or previously created Keypair
    * `key_name = "key_name = "training-keypair-ireland""`

* Create new Keypair. `public_key` required if `create_key_pair` is `true`

```hcl
module "instance" {

  create_key_pair = "true"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 sergiu.plotnicu@project-waffle.com"
}  
```  

* No Keypair at all - default Option. This option is useful when Instances are used in a imutable way either use of SSM agent to initiate session on instance. Details can be found here - [AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)



## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allocation\_strategy | Indicates how to allocate the target capacity across the Spot pools specified by the Spot fleet request. | string | `diversified` | no |
| associate\_public\_ip\_address | Associate or no Public IP to instance | string | `true` | no |
| cpu\_credits | The credit option for CPU usage. Can be standard or unlimited | string | `standard` | no |
| create\_custom\_userdata | Create custome Userdata script | string | `false` | no |
| create\_key\_pair | Create Key Pair by providing public key of own private one | string | `false` | no |
| create\_spot | set to True to create Spot instances | string | `true` | no |
| custom\_userdata\_script\_content | Custome Userdata script content - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content` | string | `` | no |
| custom\_userdata\_script\_path | Custome Userdata script path - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content` | string | `` | no |
| ebs\_optimized | If true, the launched EC2 instance will be EBS-optimized. Note that if this is not set on an instance type that is optimized by default then this will show as disabled but if the instance type is optimized by default then there is no need to set this and there is no effect to disabling it | string | `false` | no |
| enable\_encryption | Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it | string | `false` | no |
| excess\_capacity\_termination\_policy | Indicates whether running Spot instances should be terminated if the target capacity of the Spot fleet request is decreased below the current size of the Spot fleet. | string | `Default` | no |
| fleet\_type | (Optional) The type of fleet request. Indicates whether the Spot Fleet only requests the target capacity or also attempts to maintain it. | string | `maintain` | no |
| global\_tags | Global Tags | map | - | yes |
| iam\_fleet\_role | ARN needed. Grants the Spot fleet permission to terminate Spot instances on your behalf when you cancel its Spot fleet request using CancelSpotFleetRequests or when the Spot fleet request expires, if you set terminateInstancesWithExpiration. | string | `` | no |
| iam\_instance\_profile | The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile | string | `` | no |
| image\_id | Static imgae ID for input | string | `` | no |
| instance\_count | Number of Instances OS Type | string | `1` | no |
| instance\_interruption\_behaviour | (Optional) Indicates whether a Spot instance stops or terminates when it is interrupted. Default is terminate. | string | `terminate` | no |
| instance\_name\_prefix | Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1 | string | `` | no |
| instance\_os | Instance OS Type. Can be linux or windows | string | `linux` | no |
| instance\_pools\_to\_use\_count | The number of Spot pools across which to allocate your target Spot capacity. Valid only when allocation_strategy is set to lowestPrice. Spot Fleet selects the cheapest Spot pools and evenly allocates your target Spot capacity across the number of Spot pools that you specify. | string | `1` | no |
| instance\_type | Instance Type. Select instance type from approved AWS list in the region | string | `t3.nano` | no |
| key\_name | Existing AWS SSH key Name used to connect to Instance | string | `` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| public\_key | The public key material | string | `` | no |
| replace\_unhealthy\_instances | Indicates whether Spot fleet should replace unhealthy instances | string | `false` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| root\_block\_device\_ebs\_delete\_on\_termination | Whether the volume should be destroyed on instance termination | string | `true` | no |
| root\_block\_device\_ebs\_iops | The amount of provisioned IOPS for root device. This must be set with a volume_type of io1 | string | `250` | no |
| root\_block\_device\_ebs\_volume\_size | The size of the root volume in gigabytes | string | `` | no |
| root\_block\_device\_ebs\_volume\_size\_defaults | Defaults for the size of the root volume in gigabytes | map | `<map>` | no |
| root\_block\_device\_ebs\_volume\_type | The type of root volume. Can be standard, gp2, or io1 | string | `gp2` | no |
| root\_block\_device\_tags | Custome EC2 Instance Root Volume tags Tags | map | `<map>` | no |
| spot\_price | (Optional; Default: On-demand price) The maximum bid price per unit hour. | string | `` | no |
| spot\_tags | Custome EC2 Instance Tags | map | `<map>` | no |
| subnet\_az | List of subnet AZs | list | - | yes |
| subnet\_id | List of subnet IDs | list | - | yes |
| target\_capacity | The number of units to request. You can choose to set the target capacity in terms of instances or a performance characteristic that is important to your application workload, such as vCPUs, memory, or I/O | string | `1` | no |
| target\_group\_arns | A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing. | list | `<list>` | no |
| terminate\_instances\_with\_expiration | Indicates whether running Spot instances should be terminated when the Spot fleet request expires. | string | `true` | no |
| valid\_from | (Optional) The start date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ). | string | `` | no |
| valid\_until | (Optional) The end date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ). At this point, no new Spot instance requests are placed or enabled to fulfill the request. | string | `` | no |
| vpc\_security\_group\_ids | List of security groups | list | - | yes |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


