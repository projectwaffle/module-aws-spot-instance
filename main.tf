# Locals

locals {
  instance_pools_to_use_count       = "${var.allocation_strategy == "lowestPrice" ? var.instance_pools_to_use_count : ""}"
  target_capacity                   = "${var.create_spot && (var.target_capacity != "") ? var.target_capacity : 1}"
  target_capacity_per_az_base       = "${ceil(var.target_capacity / length(var.subnet_az))}"
  target_capacity_base              = "${local.target_capacity_per_az_base * length(var.subnet_az)}"
  target_capacity_minus_base        = "${var.target_capacity - local.target_capacity_base}"
  target_capacity_per_az1           = "${local.target_capacity_minus_base > 0 ? (local.target_capacity_per_az_base + 1) : local.target_capacity_per_az_base}"
  target_capacity_per_az2           = "${local.target_capacity_minus_base > 1 ? (local.target_capacity_per_az_base + 1) : local.target_capacity_per_az_base}"
  target_capacity_per_az3           = "${local.target_capacity_minus_base > 2 ? (local.target_capacity_per_az_base + 1) : local.target_capacity_per_az_base}"
  target_capacity_per_az            = "${list(local.target_capacity_per_az1, local.target_capacity_per_az2, local.target_capacity_per_az3)}"
  valid_until                       = "${var.valid_until != "" ? var.valid_until : timeadd(timestamp(), "24h")}"
  valid_from                        = "${var.valid_from != "" ? var.valid_from : timestamp()}"
  iam_fleet_role                    = "${var.iam_fleet_role != "" ? var.iam_fleet_role : ""}"
  root_block_device_ebs_iops        = "${(var.root_block_device_ebs_volume_type == "io1") && (var.root_block_device_ebs_iops != 0) ? var.root_block_device_ebs_iops : 0}"
  root_block_device_ebs_volume_size = "${var.root_block_device_ebs_volume_size != "" ? var.root_block_device_ebs_volume_size : lookup(var.root_block_device_ebs_volume_size_defaults, var.instance_os, 8)}"
  instance_ids                      = "${concat(aws_instance.main.*.id, list(""))}"
  image_id                          = "${var.image_id != "" ? var.image_id : lookup(map("linux", data.aws_ami.linux.id, "windows", data.aws_ami.windows.id), var.instance_os, data.aws_ami.linux.id)}"
  instance_name_prefix              = "${lower(var.instance_name_prefix != "" ? var.instance_name_prefix : var.name)}"
  iam_instance_profile              = "${var.iam_instance_profile != "" ? var.iam_instance_profile : ""}"
  key_name                          = "${element(concat(aws_key_pair.main.*.key_name, list(var.key_name), list("")), 0)}"
  template_cloudinit_config_main    = "${concat(data.template_cloudinit_config.main.*.rendered, list(""))}"
  template_cloudinit_config_custom  = "${element(concat(data.template_file.custom_userdata.*.rendered, list(var.custom_userdata_script_content), list("")), 0)}"
}

## Resources

# Resource Key Pair

resource "aws_key_pair" "main" {
  count = "${var.create_spot && var.create_key_pair ? 1 : 0 }"

  key_name   = "${format("%s-%s", var.name, var.resource_identities["key_name"],)}"
  public_key = "${var.public_key}"
}

# Resource EC2 instance UserData input

/*
resource "random_integer" "subnet_count" {
  count = "${var.create_spot ? var._count : 0 }"

  min = 0
  max = "${length(var.subnet_id) == "1" ? 1 : (length(var.subnet_id) - 1) }"
}
*/

data "template_file" "custom_userdata" {
  count = "${var.create_spot && var.create_custom_userdata && var.custom_userdata_script_path != "" ? 1 : 0 }"

  template = "${file("${var.custom_userdata_script_path}")}"
}

# Spot Request

resource "aws_spot_fleet_request" "main" {
  count = "${var.create_spot && var.target_capacity > 0 ? length(var.subnet_az) : 0}"

  iam_fleet_role                      = "${local.iam_fleet_role}"
  replace_unhealthy_instances         = "${var.replace_unhealthy_instances}"
  spot_price                          = "${var.spot_price}"
  target_capacity                     = "${element(local.target_capacity_per_az, count.index)}"
  allocation_strategy                 = "${var.allocation_strategy}"
  instance_pools_to_use_count         = "${var.instance_pools_to_use_count}"
  excess_capacity_termination_policy  = "${var.excess_capacity_termination_policy}"
  terminate_instances_with_expiration = "${var.terminate_instances_with_expiration}"
  instance_interruption_behaviour     = "${var.instance_interruption_behaviour}"
  fleet_type                          = "${var.fleet_type}"
  valid_until                         = "${local.valid_until}"
  valid_from                          = "${local.valid_from}"
  target_group_arns                   = ["${var.target_group_arns}"]

  launch_specification {
    ami                         = "${local.image_id}"
    ebs_optimized               = "${var.ebs_optimized}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${local.key_name}"
    vpc_security_group_ids      = ["${var.vpc_security_group_ids}"]
    subnet_id                   = "${element(var.subnet_id, count.index)}"
    user_data                   = "${var.create_custom_userdata ? local.template_cloudinit_config_custom : ""}"
    iam_instance_profile        = "${local.iam_instance_profile}"
    associate_public_ip_address = "${var.associate_public_ip_address}"

    root_block_device {
      volume_type           = "${var.root_block_device_ebs_volume_type}"
      volume_size           = "${local.root_block_device_ebs_volume_size}"
      iops                  = "${local.root_block_device_ebs_iops}"
      delete_on_termination = "${var.root_block_device_ebs_delete_on_termination}"
    }

    tags = "${merge(map("Name", format("%s%d", local.instance_name_prefix, (count.index + 1))), var.global_tags, var.module_tags, var.spot_tags)}"
  }

  lifecycle {
    ignore_changes = ["user_data", "tags", "private_ip", "ami", "root_block_device", "ebs_block_device", "valid_until", "valid_from"]
  }
}
