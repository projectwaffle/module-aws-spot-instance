<powershell>
  netsh advfirewall firewall add rule name="WinRM in" protocol=TCP dir=in profile=any localport=5985 remoteip=any localip=any action=allow
  # Rename PC
  Rename-Computer -NewName "${hostname}" -Restart
</powershell>
