## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# Key Pair

variable "key_name" {
  description = "Existing AWS SSH key Name used to connect to Instance"
  default     = ""
}

variable "public_key" {
  description = "The public key material"
  default     = ""
}

variable "create_key_pair" {
  description = "Create Key Pair by providing public key of own private one"
  default     = "false"
}

# Spot Instance

variable "create_spot" {
  description = "set to True to create Spot instances"
  default     = "true"
}

variable "iam_fleet_role" {
  description = "ARN needed. Grants the Spot fleet permission to terminate Spot instances on your behalf when you cancel its Spot fleet request using CancelSpotFleetRequests or when the Spot fleet request expires, if you set terminateInstancesWithExpiration."
  default     = ""
}

variable "replace_unhealthy_instances" {
  description = "Indicates whether Spot fleet should replace unhealthy instances"
  default     = "false"
}

variable "spot_price" {
  description = "(Optional; Default: On-demand price) The maximum bid price per unit hour."
  default     = ""
}

variable "target_capacity" {
  description = "The number of units to request. You can choose to set the target capacity in terms of instances or a performance characteristic that is important to your application workload, such as vCPUs, memory, or I/O"
  default     = "1"
}

variable "allocation_strategy" {
  description = "Indicates how to allocate the target capacity across the Spot pools specified by the Spot fleet request."
  default     = "diversified"
}

variable "instance_pools_to_use_count" {
  description = "The number of Spot pools across which to allocate your target Spot capacity. Valid only when allocation_strategy is set to lowestPrice. Spot Fleet selects the cheapest Spot pools and evenly allocates your target Spot capacity across the number of Spot pools that you specify."
  default     = "1"
}

variable "excess_capacity_termination_policy" {
  description = "Indicates whether running Spot instances should be terminated if the target capacity of the Spot fleet request is decreased below the current size of the Spot fleet."
  default     = "Default"
}

variable "terminate_instances_with_expiration" {
  description = "Indicates whether running Spot instances should be terminated when the Spot fleet request expires."
  default     = "true"
}

variable "instance_interruption_behaviour" {
  description = "(Optional) Indicates whether a Spot instance stops or terminates when it is interrupted. Default is terminate."
  default     = "terminate"
}

variable "fleet_type" {
  description = "(Optional) The type of fleet request. Indicates whether the Spot Fleet only requests the target capacity or also attempts to maintain it. "
  default     = "maintain"
}

variable "valid_until" {
  description = "(Optional) The end date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ). At this point, no new Spot instance requests are placed or enabled to fulfill the request."
  default     = ""
}

variable "valid_from" {
  description = "(Optional) The start date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ)."
  default     = ""
}

variable "target_group_arns" {
  description = "A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing."
  default     = []
}

# Spot launch instance configuration

variable "instance_os" {
  description = "Instance OS Type. Can be linux or windows"
  default     = "linux"
}

variable "instance_count" {
  description = "Number of Instances OS Type"
  default     = "1"
}

variable "associate_public_ip_address" {
  description = "Associate or no Public IP to instance"
  default     = "true"
}

variable "enable_encryption" {
  description = "Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it"
  default     = "false"
}

variable "instance_type" {
  description = "Instance Type. Select instance type from approved AWS list in the region"
  default     = "t3.nano"
}

variable "instance_name_prefix" {
  description = "Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1"
  default     = ""
}

variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized. Note that if this is not set on an instance type that is optimized by default then this will show as disabled but if the instance type is optimized by default then there is no need to set this and there is no effect to disabling it"
  default     = "false"
}

variable "cpu_credits" {
  description = "The credit option for CPU usage. Can be standard or unlimited"
  default     = "standard"
}

variable "vpc_security_group_ids" {
  description = "List of security groups"
  type        = "list"
}

variable "subnet_id" {
  description = "List of subnet IDs"
  type        = "list"
}

variable "subnet_az" {
  description = "List of subnet AZs"
  type        = "list"
}

variable "create_custom_userdata" {
  description = "Create custome Userdata script"
  default     = "false"
}

variable "custom_userdata_script_path" {
  description = "Custome Userdata script path - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content`"
  default     = ""
}

variable "custom_userdata_script_content" {
  description = "Custome Userdata script content - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content`"
  default     = ""
}

variable "image_id" {
  description = "Static imgae ID for input"
  default     = ""
}

variable "spot_tags" {
  description = "Custome EC2 Instance Tags"
  default     = {}
}

# Root EBS Volume

variable "root_block_device_ebs_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination"
  default     = "true"
}

variable "root_block_device_ebs_volume_size_defaults" {
  description = "Defaults for the size of the root volume in gigabytes"

  default = {
    linux   = "8"
    windows = "32"
  }
}

variable "root_block_device_ebs_volume_size" {
  description = "The size of the root volume in gigabytes"
  default     = ""
}

variable "root_block_device_ebs_volume_type" {
  description = "The type of root volume. Can be standard, gp2, or io1"
  default     = "gp2"
}

variable "root_block_device_ebs_iops" {
  description = "The amount of provisioned IOPS for root device. This must be set with a volume_type of io1"
  default     = "250"
}

variable "root_block_device_tags" {
  description = "Custome EC2 Instance Root Volume tags Tags"
  default     = {}
}

variable "iam_instance_profile" {
  description = " The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile"
  default     = ""
}
